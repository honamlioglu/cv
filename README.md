# Contact & Personal
• Phone: +905338438623<br>
• Nationality: Turk<br>
• Birth of Year: 1994<br>
• Skype: live:honamliogluozan<br>
• Mail: ozanhonamlioglu@gmail.com<br>
• Github: [Personal github page](https://github.com/ozanhonamlioglu)<br>
• Cypchat Github: [Cypchat application's github page](https://github.com/cypchat)<br>
• NPM: [My open source javascript libraries for NPM](https://www.npmjs.com/~xmastree)<br>

# Who Am I (Mobile & Web Developer)
<b>Owner of Cypchat online dating application for iOS & Android.</b><br>
• IOS app is written in Swift. [IOS App download link](https://apps.apple.com/app/cypchat/id1473050955)<br>
• Android app is written in React Native, Redux, GraphQL. [Android App download link](https://play.google.com/store/apps/details?id=com.cypchat.dev)<br>
• Web page is written in React, Redux [cypchat.com](https://cypchat.com/)<br>
<b>You can also download and test the app with given sample user information: </b>
- Email: daria@test.com
- Password: qwerty

I am mainly a <b>Swift programmer for IOS and (NodeJs & TypeScript & React Technologies) programmer.</b><br>
So that I am able to make <b>Mobile applications and Web Sites since 2011</b> with professional knowledge.

# Years that I Spent in Coding
• I Started at high school when I was 14-15 years old.<br>
• Professinally started at 2012 which is University times.

# Skills

```mermaid
graph LR

Skill1[Mobile Development]
Skill1 --> Value16(Swift)
Skill1 --> Value11(React Native)

Skill2[Web Development]
Skill2 --> Value21(NodeJS)
Skill2 --> Value291(TypeScript)
Skill2 --> JS(JavaScript ES6)
JS --> F(Frameworks)
F --> Value24(React)
F --> Value26(Redux)
F --> Value295(GraphQL)
F --> Value296(NextJs)
F --> Value297(React Native)

Skill7[DevOPS]
Skill7 --> Value71(Docker)
Skill7 --> Value72(Kubernetes)
Skill7 --> Value73(Travis CI)
Skill7 --> Value74(Git: Gitlab & Github)

Skill4[Database Knowledge]
Skill4 --> Value42(MongoDB)
Skill4 --> Value44(FireBase)
Skill4 --> Value41(MySql)
Skill4 --> Value43(PostgreSQL)

Skill3[Bonus]
Skill3 --> Value31(Python)
Skill3 --> Value32(C++)
Skill3 --> Value33(Objective-c)
Skill3 --> Value34(Buildroot Kernel Compiling)
```

# Work History
### 1) Online Bet Front End Developer @ Malpas Casino (5 Months)
<b>Technologies that I used in the company:</b> AngularJs, NodeJs, Python, Docker<br>
I have been developing an Online Betting systems for hotel casino with AngularJs & NodeJs & Python.

### 2) Mobile & Web Developer @ ISVIC İletişim (1 Year 3 Months)
<b>Technologies that I used in the company:</b> Swift, Objective-c, React Native, ReactJs, NodeJs, Python, Docker<br>
Worked as Swift and JavaScript developer. I respond too many different requests from customers with my updated javascript knowledge such as making desktop applications <b>with Electron.js</b> <br>
Also I was responsible of a native banking's web site in Cyprus named vakiflarbankasi. I managed its server and contributed some of the websites codes whenever they requested from my company.<br>
<b>Some of my works at the company</b><br>
• 10totravel (Jquery, Python Django, Nginx) [Online booking system](https://www.10totravel.com/)<br>
• KKTC panik butonu (Swift) [A panic button for unprotected people](https://apps.apple.com/tr/app/kktc-panik-butonu/id1297949857?l=tr)<br>
• NCYPAPP (Objective-c) [I managed the already written app and contribute its code](https://apps.apple.com/app/north-cyprus-application/id1183869923)<br>
• KKTC doctors (React, Redux, PHP) [Turkish medical association](http://kibristurktabipleriodasi.org/)<br>
• Customer Tracker Desktop Application & Mobile Application (React, Redux, Electron.js, Node.js, React-Native)<br>
• <b>Important</b> All the front-end designs are sketched by a co-worker and then coded by me.

### 3) NodeJs & C++ Developer @ GUNSEL EV (1 Years 5 Months) Currently Working...
<b>Technologies that I used in the company:</b> NodeJs, ReactJs, C++, Python, Docker, Kubernetes<br>
Currently i am working in an Electrical Vehicle Company named GUNSEL [here is website](https://gunsel.com.tr/)<br>
Main bussiness at the company to help to develop Digital Instrument Cluster and develop web based application for company.<br>
<b>Some of my works at the company</b><br>
• Helping to develop GUNSEL's EV DIC with C++<br>
• NodeJs & ReactJs & Python powered telemetry<br>
• ReactNative powered car data tracker<br>
• I am able to develop custom fast booted Raspberry-pi kernel via Buildroot which boots up under 3 seconds and starts to run an application on the screen.<br>












